//
//  FruitData.swift
//  FruitsApp
//
//  Created by Karim Razhanov on 23.02.2020.
//  Copyright © 2020 Karim Razhanov. All rights reserved.
//

import UIKit

class FruitData: Codable {
    let id: Int
    let text: String
    let image: String
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case text = "text"
        case image = "image"
    }
}
