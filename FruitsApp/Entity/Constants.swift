//
//  Constants.swift
//  FruitsApp
//
//  Created by Karim Razhanov on 24.02.2020.
//  Copyright © 2020 Karim Razhanov. All rights reserved.
//

import UIKit

struct Constants {
    static let fruitsTitleText = "Фруктовый сад"
    static let myFruitsText = "Мои фрукты"
    static let allFruitsText = "Все фрукты"
    
    static let myFruitCellIdentifier = "MyFruitTableViewCell"
    static let fruitCollectionViewCellIdentifier = "FruitCollectionViewCell"
    
    static let errorTitle = "Error!"
    static let reloadText = "Reload"
    static let addAgainAlertTitle = "Fruit didn't added!"
    static let removeAgainAlertTitle = "Fruit didn't deleted!"
    static let tryAgainText = "Try again!"
}
