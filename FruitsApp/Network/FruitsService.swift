//
//  FruitsService.swift
//  FruitsApp
//
//  Created by Karim Razhanov on 23.02.2020.
//  Copyright © 2020 Karim Razhanov. All rights reserved.
//

import UIKit

class FruitsService {

    static func getAllFruits(completionHandler: @escaping (Result<[FruitData], Error>) -> Void) {
        let factory = RequestFactory()
        var fruitsFactory: FruitsRequestFactory?
        
        fruitsFactory = factory.makeFruitsRequestFactory()
        fruitsFactory?.getAllFruits(completion: { (result) in
            switch result {
            case .success(let allFruits):
                completionHandler(.success(allFruits))
            case .failure(let error):
                completionHandler(.failure(error))
            }
        })
    }
    
    static func getMyFruits(completionHandler: @escaping (Result<[FruitData], Error>) -> Void) {
        let factory = RequestFactory()
        var fruitsFactory: FruitsRequestFactory?
        
        fruitsFactory = factory.makeFruitsRequestFactory()
        fruitsFactory?.getMyFruits(completion: { (result) in
            switch result {
            case .success(let myFruits):
                completionHandler(.success(myFruits))
            case .failure(let error):
                completionHandler(.failure(error))
            }
        })
    }
    
    static func addNewFruit(id: Int, completionHandler: @escaping (Error?) -> Void) {
        let factory = RequestFactory()
        var fruitsFactory: FruitsRequestFactory?
        
        fruitsFactory = factory.makeFruitsRequestFactory()
        fruitsFactory?.addNewFruit(id, completion: { (error) in
            completionHandler(error)
        })
    }
    
    static func removeFruit(id: Int, completionHandler: @escaping (Error?) -> Void) {
        let factory = RequestFactory()
        var fruitsFactory: FruitsRequestFactory?
        
        fruitsFactory = factory.makeFruitsRequestFactory()
        fruitsFactory?.removeFruit(id, completion: { (error) in
            completionHandler(error)
        })
    }
}
