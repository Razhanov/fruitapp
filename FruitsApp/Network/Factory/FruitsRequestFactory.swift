//
//  FruitsRequestFactory.swift
//  FruitsApp
//
//  Created by Karim Razhanov on 23.02.2020.
//  Copyright © 2020 Karim Razhanov. All rights reserved.
//

import UIKit
import Alamofire

final class FruitsRequestFactory: AbstractRequestFactory {
    var sessionManager: SessionManager
    
    var queue: DispatchQueue
    
    init(sessionManager: SessionManager, queue: DispatchQueue = DispatchQueue.global(qos: .userInitiated)) {
        self.sessionManager = sessionManager
        self.queue = queue
    }
    
    public func getAllFruits(completion: @escaping (Result<[FruitData]>) -> Void) {
        let request = FruitsRequestRouter.getAllFruits
        self.request(request).responseJSON { (response) in
            guard let data = response.data, !response.result.isFailure else {
                if let error = response.result.error {
                    completion(.failure(error))
                }
                return
            }
            if let allFruits = try? JSONDecoder().decode([FruitData].self, from: data) {
                completion(.success(allFruits))
                return
            }
            if let error = response.error {
                completion(.failure(error))
            }
        }
        
    }
    
    public func getMyFruits(completion: @escaping (Result<[FruitData]>) -> Void) {
        let request = FruitsRequestRouter.getMyFruits(uid: 16)
        self.request(request).responseJSON { (response) in
            guard let data = response.data, !response.result.isFailure else {
                if let error = response.result.error {
                    completion(.failure(error))
                }
                return
            }
            if let myFruits = try? JSONDecoder().decode([FruitData].self, from: data) {
                completion(.success(myFruits))
                return
            }
            if let error = response.error {
                completion(.failure(error))
            }
        }
        
    }
    
    public func addNewFruit(_ id: Int, completion: @escaping (Error?) -> Void) {
        let parameters: Parameters = ["fruit_id" : "\(id)"]
        let request = FruitsRequestRouter.addFruit(uid: 16, parameters: parameters)
        self.request(request).responseData { (response) in
            guard let _ = response.data, !response.result.isFailure else {
                if let error = response.result.error {
                    completion(error)
                }
                return
            }
            if let error = response.error {
                completion(error)
            }
            completion(nil)
        }
    }
    
    public func removeFruit(_ id: Int, completion: @escaping (Error?) -> Void) {
        let parameters: Parameters = ["fruit_id" : "\(id)"]
        let request = FruitsRequestRouter.removeFruit(uid: 16, parameters: parameters)
        self.request(request).responseData { (response) in
            guard let _ = response.data, !response.result.isFailure else {
                if let error = response.result.error {
                    completion(error)
                }
                return
            }
            if let error = response.error {
                completion(error)
            }
            completion(nil)
        }
    }
    
    
}
