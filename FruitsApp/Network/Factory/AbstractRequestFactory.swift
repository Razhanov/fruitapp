//
//  AbstractRequestFactory.swift
//  FruitsApp
//
//  Created by Karim Razhanov on 23.02.2020.
//  Copyright © 2020 Karim Razhanov. All rights reserved.
//

import UIKit
import Alamofire

protocol AbstractRequestFactory {
    var sessionManager: SessionManager { get }
    var queue: DispatchQueue { get }
    
    @discardableResult
    func request(_ request: URLRequestConvertible) -> DataRequest
}

extension AbstractRequestFactory {
    @discardableResult
    func request(_ request: URLRequestConvertible) -> DataRequest {
            return sessionManager.request(request)
    }
}
