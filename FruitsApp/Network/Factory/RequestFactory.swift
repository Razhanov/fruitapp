//
//  RequestFactory.swift
//  FruitsApp
//
//  Created by Karim Razhanov on 23.02.2020.
//  Copyright © 2020 Karim Razhanov. All rights reserved.
//

import UIKit
import Alamofire

class RequestFactory {
    
    private var fruitsFactory: FruitsRequestFactory?

    lazy private var sessionManager: SessionManager = {
        return SessionManagerFactory.sessionManager
    }()
    
    func makeFruitsRequestFactory() -> FruitsRequestFactory {
        if let factory = fruitsFactory {
            return factory
        } else {
            let factory = FruitsRequestFactory(sessionManager: sessionManager)
            fruitsFactory = factory
            return factory
        }
    }
    
}
