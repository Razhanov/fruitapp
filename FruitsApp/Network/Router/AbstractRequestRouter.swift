//
//  AbstractRequestRouter.swift
//  FruitsApp
//
//  Created by Karim Razhanov on 23.02.2020.
//  Copyright © 2020 Karim Razhanov. All rights reserved.
//

import UIKit
import Alamofire

protocol AbstractRequestRouter: URLRequestConvertible {
    var baseUrl: URL { get }
    var fullUrl: URL { get }
    var path: String { get }
    var headers: HTTPHeaders { get }
    var method: HTTPMethod { get }
}

extension AbstractRequestRouter {
    var baseUrl: URL {
        let string = "http://vsibi.best/api/test"
        return URL.init(string: string)!
    }
    
    var fullUrl: URL {
        return baseUrl.appendingPathComponent(path)
    }
    
    var headers: HTTPHeaders {
        return ["Content-Type": "application/json"]
    }
    
}
