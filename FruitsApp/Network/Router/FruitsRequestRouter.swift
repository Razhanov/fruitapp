//
//  FruitsRequestRouter.swift
//  FruitsApp
//
//  Created by Karim Razhanov on 23.02.2020.
//  Copyright © 2020 Karim Razhanov. All rights reserved.
//

import UIKit
import Alamofire

enum FruitsRequestRouter: AbstractRequestRouter {

    case getAllFruits
    case getMyFruits(uid: Int)
    case addFruit(uid: Int, parameters: Parameters)
    case removeFruit(uid: Int, parameters: Parameters)
    
    var path: String {
        switch self {
        case .getAllFruits:
            return "/all"
        case .getMyFruits:
            return "/my_fruits"
        case .addFruit:
            return "/add"
        case .removeFruit:
            return "/remove"
        }
    }
    
    var headers: HTTPHeaders {
        switch self {
        case .getAllFruits:
            return [:]
        case .getMyFruits(let uid):
            return ["uid" : "\(uid)"]
        case .addFruit(let uid,_ ):
            return ["uid" : "\(uid)"]
        case .removeFruit(uid: let uid, _):
            return ["uid" : "\(uid)"]
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .getAllFruits, .getMyFruits:
            return .get
        case .addFruit, .removeFruit:
            return .post
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = URLRequest(url: fullUrl)
        urlRequest.httpMethod = method.rawValue
        urlRequest.allHTTPHeaderFields = headers
        
        switch self {
        case .getAllFruits, .getMyFruits:
            urlRequest = try URLEncoding.default.encode(urlRequest, with: nil)
        case .addFruit(_ , let parameters), .removeFruit(_ , let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        }
        return urlRequest
    }
}
