//
//  UIImageView + Network.swift
//  FruitsApp
//
//  Created by Karim Razhanov on 23.02.2020.
//  Copyright © 2020 Karim Razhanov. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

let imageCache = AutoPurgingImageCache(memoryCapacity: 100_000_000, preferredMemoryUsageAfterPurge: 60_000_000)

extension UIImageView {
    
    func loadWithAlamofire(urlString: String, placeholderImage: UIImage = UIImage(imageLiteralResourceName: "placeholder"), completion: @escaping () -> Void = { }) {
        if let cachedImage = imageCache.image(withIdentifier: urlString) {
            self.image = cachedImage
            completion()
        } else {
            Alamofire.request(urlString).responseImage { response in
                if let imageUrl = response.result.value {
                    self.image = imageUrl
                    imageCache.add(imageUrl, withIdentifier: urlString)
                    completion()
                } else {
                    self.image = placeholderImage
                    completion()
                }
            }
        }
    }
    
}
