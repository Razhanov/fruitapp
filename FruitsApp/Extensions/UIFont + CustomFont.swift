//
//  UIFont + CustomFont.swift
//  FruitsApp
//
//  Created by Karim Razhanov on 23.02.2020.
//  Copyright © 2020 Karim Razhanov. All rights reserved.
//

import UIKit

extension UIFont {
    class func roboto(ofSize size: CGFloat, style: UIFont.Weight) -> UIFont? {
        switch style {
        case .bold:
            return UIFont(name: "Roboto-Bold", size: size)
        case .regular:
            return UIFont(name: "Roboto-Regular", size: size)
        case .medium:
            return UIFont(name: "Roboto-Medium", size: size)
        case .black:
            return UIFont(name: "Roboto-Black", size: size)
        default:
            return UIFont(name: "Roboto-Regular", size: size)
        }
    }
}
