//
//  UIColor + CustomColor.swift
//  FruitsApp
//
//  Created by Karim Razhanov on 24.02.2020.
//  Copyright © 2020 Karim Razhanov. All rights reserved.
//

import UIKit

extension UIColor {
    static var fruitsGreen: UIColor {
        return UIColor(red: 0.412, green: 0.588, blue: 0.208, alpha: 1)
    }
    
    static var fruitCellViewBackgroundColor: UIColor {
        return UIColor(red: 0.946, green: 0.946, blue: 0.946, alpha: 1)
    }
    
    static var allFruitsBackgroundColor: UIColor {
        return UIColor(red: 0.804, green: 0.804, blue: 0.804, alpha: 1)
    }
}
