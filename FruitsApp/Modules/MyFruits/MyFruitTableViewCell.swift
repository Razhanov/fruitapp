//
//  MyFruitTableViewCell.swift
//  FruitsApp
//
//  Created by Karim Razhanov on 23.02.2020.
//  Copyright © 2020 Karim Razhanov. All rights reserved.
//

import UIKit

class MyFruitTableViewCell: UITableViewCell {
    
    private(set) lazy var myFruitCellView: MyFruitCellView = {
        let myFruitCellView = MyFruitCellView()
        return myFruitCellView
    }()

    
    func display(_ fruitData: FruitData) {
        myFruitCellView.update(with: fruitData)
        selectionStyle = .none
        backgroundColor = .fruitsGreen
        addSubviews()
        makeConstraints()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    private func addSubviews() {
        addSubview(myFruitCellView)
    }
    
    private func makeConstraints() {
        myFruitCellView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }

}
