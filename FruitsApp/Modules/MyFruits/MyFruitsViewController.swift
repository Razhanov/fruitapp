//
//  MyFruitsViewController.swift
//  FruitsApp
//
//  Created by Karim Razhanov on 23.02.2020.
//  Copyright © 2020 Karim Razhanov. All rights reserved.
//

import UIKit
import SnapKit
import UBottomSheet

extension MyFruitsViewController {
    struct Appearance {
        let titleLabelSize: CGFloat = 21
        let tableCornerRadius: CGFloat = 20
        let tableBottomInset: CGFloat = 80
        let titleLabelTopOffset: CGFloat = 66
        let titleLabelLeadingOffset: CGFloat = 29
        let tableViewTopOffset: CGFloat = 15
        
        let heightForRow: CGFloat = 114
        let headerHeight: CGFloat = 100
    }
}

class MyFruitsViewController: UIViewController {
    
    var configurator = MyFruitsConfiguratorImplementation()
    var presenter: MyFruitsPresenter?
    
    private let appearance = Appearance()
    
    private(set) lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.roboto(ofSize: appearance.titleLabelSize, style: .medium)
        label.text = Constants.fruitsTitleText
        return label
    }()
    
    private(set) lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(MyFruitTableViewCell.self, forCellReuseIdentifier: Constants.myFruitCellIdentifier)
        tableView.backgroundColor = .fruitsGreen
        tableView.layer.cornerRadius = appearance.tableCornerRadius
        tableView.clipsToBounds = true
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: appearance.tableBottomInset, right: 0)
        tableView.contentInsetAdjustmentBehavior = .never
        return tableView
    }()
    
    private(set) lazy var allFruitsViewController: AllFruitsViewController = {
        let allFruitsVC = AllFruitsViewController()
        allFruitsVC.configurator = AllFruitsConfiguratorImplementation(allFruitsPresenterDelegate: presenter as? AllFruitsPresenterDelegate)
        return allFruitsVC
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.setNavigationBarHidden(true, animated: false)
        configurator.configure(myFruitsViewController: self)
        presenter?.viewDidLoad()
    }
    

}

extension MyFruitsViewController: MyFruitsView {
    func showReloadAlert(errorText: String) {
        let ac = UIAlertController(title: Constants.errorTitle, message: errorText, preferredStyle: .alert)
        let reloadAction = UIAlertAction(title: Constants.reloadText, style: .default) { [weak self] _ in
            self?.presenter?.fetchData()
        }
        ac.addAction(reloadAction)
        present(ac, animated: true, completion: nil)
    }
    
    func removeFruitAgainAlert(errorText: String, row: Int) {
        let ac = UIAlertController(title: Constants.removeAgainAlertTitle, message: errorText, preferredStyle: .alert)
        let reloadAction = UIAlertAction(title: Constants.tryAgainText, style: .default) { [weak self] _ in
            self?.presenter?.didSelect(row: row)
        }
        ac.addAction(reloadAction)
        present(ac, animated: true, completion: nil)
    }
    
    func setView() {
        view.addSubview(titleLabel)
        view.addSubview(tableView)
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(appearance.titleLabelTopOffset)
            make.leading.equalToSuperview().offset(appearance.titleLabelLeadingOffset)
            make.trailing.equalToSuperview().inset(appearance.titleLabelLeadingOffset)
        }
        tableView.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(appearance.tableViewTopOffset)
            make.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    func refreshFruitsData() {
        tableView.reloadData()
    }
    
    func addAllFruitsVC() {
        allFruitsViewController.attach(to: self)
    }
}

extension MyFruitsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.numberOfMyFruits ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.myFruitCellIdentifier, for: indexPath) as? MyFruitTableViewCell else {
            let cell = MyFruitTableViewCell(style: .default, reuseIdentifier: Constants.myFruitCellIdentifier)
            presenter?.configure(cell: cell, forRow: indexPath.row)
            return cell
        }
        presenter?.configure(cell: cell, forRow: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return appearance.heightForRow
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return Constants.myFruitsText
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let titleLabel = UILabel()
        titleLabel.frame =  CGRect(x: 30, y: 30, width: UIScreen.main.bounds.size.width - 60, height: 30)
        titleLabel.font = UIFont.roboto(ofSize: appearance.titleLabelSize, style: .medium)
        titleLabel.text = self.tableView(tableView, titleForHeaderInSection: section)
        titleLabel.textColor = .white

        let headerView:UIView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: tableView.frame.size.height))
        headerView.addSubview(titleLabel)
        headerView.backgroundColor = .fruitsGreen
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return appearance.headerHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.didSelect(row: indexPath.row)
    }
    
}

