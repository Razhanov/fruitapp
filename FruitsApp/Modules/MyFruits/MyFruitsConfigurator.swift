//
//  MyFruitsConfigurator.swift
//  FruitsApp
//
//  Created by Karim Razhanov on 23.02.2020.
//  Copyright © 2020 Karim Razhanov. All rights reserved.
//

import UIKit

protocol MyFruitsConfigurator {
    func configure(myFruitsViewController: MyFruitsViewController)
}

class MyFruitsConfiguratorImplementation: MyFruitsConfigurator {

    func configure(myFruitsViewController: MyFruitsViewController) {
        let presenter = MyFruitsPresenterImplementation(view: myFruitsViewController)
        myFruitsViewController.presenter = presenter
        
    }
}
