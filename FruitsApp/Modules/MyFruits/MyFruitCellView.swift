//
//  MyFruitCellView.swift
//  FruitsApp
//
//  Created by Karim Razhanov on 23.02.2020.
//  Copyright © 2020 Karim Razhanov. All rights reserved.
//

import UIKit
import SnapKit

extension MyFruitCellView {
    struct Appearance {
        let viewCornerRadius: CGFloat = 10
        let fruitNameLabelSize: CGFloat = 21
        
        let viewLeadingOffset: CGFloat = 29
        let viewTrailingInset: CGFloat = 30
        let viewBottomInset: CGFloat = 14
        let fruitImageViewLeadingOffset: CGFloat = 20
        let fruitImageViewWidth: CGFloat = 60
        let fruitNameLabelLeadingOffset: CGFloat = 103
    }
}

class MyFruitCellView: UIView {
    
    private let appearance = Appearance()

    private(set) lazy var view: UIView = {
        let view = UIView()
        view.backgroundColor = .fruitCellViewBackgroundColor
        view.layer.cornerRadius = appearance.viewCornerRadius
        view.clipsToBounds = true
        return view
    }()
    
    private(set) lazy var fruitImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private(set) lazy var fruitNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.roboto(ofSize: appearance.fruitNameLabelSize, style: .medium)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubviews()
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        addSubviews()
        makeConstraints()
    }
    
    func update(with fruit: FruitData) {
        fruitNameLabel.text = fruit.text
        fruitImageView.loadWithAlamofire(urlString: fruit.image)
    }
    
    private func addSubviews() {
        addSubview(view)
        view.addSubview(fruitImageView)
        view.addSubview(fruitNameLabel)
    }
    
    private func makeConstraints() {
        view.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(appearance.viewLeadingOffset)
            make.trailing.equalToSuperview().inset(appearance.viewTrailingInset)
            make.top.equalToSuperview()
            make.bottom.equalToSuperview().inset(appearance.viewBottomInset)
        }
        fruitImageView.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(appearance.fruitImageViewLeadingOffset)
            make.width.equalTo(appearance.fruitImageViewWidth)
            make.top.bottom.equalToSuperview()
        }
        fruitNameLabel.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(appearance.fruitNameLabelLeadingOffset)
            make.centerY.equalToSuperview()
            make.trailing.lessThanOrEqualToSuperview()
        }
    }
}
