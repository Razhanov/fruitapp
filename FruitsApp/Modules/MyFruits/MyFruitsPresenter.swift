//
//  MyFruitsPresenter.swift
//  FruitsApp
//
//  Created by Karim Razhanov on 23.02.2020.
//  Copyright © 2020 Karim Razhanov. All rights reserved.
//

import UIKit

protocol MyFruitsView: class {
    func setView()
    func showReloadAlert(errorText: String)
    func removeFruitAgainAlert(errorText: String, row: Int)
    func refreshFruitsData()
    func addAllFruitsVC()
}

protocol MyFruitsPresenter {
    var numberOfMyFruits: Int { get }
    func viewDidLoad()
    func fetchData()
    func configure(cell: MyFruitTableViewCell, forRow row: Int)
    func didSelect(row: Int)
}

class MyFruitsPresenterImplementation: MyFruitsPresenter {
    
    var fruits = [FruitData]()
    
    var numberOfMyFruits: Int {
        return fruits.count
    }

    fileprivate weak var view: MyFruitsView?
    
    init(view: MyFruitsView) {
        self.view = view
    }
    
    func viewDidLoad() {
        view?.setView()
        view?.addAllFruitsVC()
        fetchData()
    }
    
    func fetchData() {
        FruitsService.getMyFruits { [weak self] result in
            switch result {
            case .success(let myFruits):
                self?.handleRecievedData(myFruits)
            case .failure(let error):
                self?.view?.showReloadAlert(errorText: error.localizedDescription)
            }
        }
    }
    
    fileprivate func handleRecievedData(_ fruits: [FruitData]) {
        self.fruits = fruits
        view?.refreshFruitsData()
    }
    
    func configure(cell: MyFruitTableViewCell, forRow row: Int) {
        cell.display(fruits[row])
    }
    
    func didSelect(row: Int) {
        FruitsService.removeFruit(id: fruits[row].id) { [weak self] (error) in
            if let error = error {
                self?.view?.removeFruitAgainAlert(errorText: error.localizedDescription, row: row)
            } else {
                self?.fetchData()
            }
        }
    }
}

extension MyFruitsPresenterImplementation: AllFruitsPresenterDelegate {
    func addNewFruit(_ fruit: FruitData) {
        fetchData()
    }
}
