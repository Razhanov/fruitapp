//
//  FruitCellView.swift
//  FruitsApp
//
//  Created by Karim Razhanov on 23.02.2020.
//  Copyright © 2020 Karim Razhanov. All rights reserved.
//

import UIKit

extension FruitCellView {
    struct Appearance {
        let viewCornerRadius: CGFloat = 10
        let imageViewWidthHeight: CGFloat = 60
    }
}

class FruitCellView: UIView {

    private let appearance = Appearance()

    private(set) lazy var view: UIView = {
        let view = UIView()
        view.backgroundColor = .fruitCellViewBackgroundColor
        view.layer.cornerRadius = appearance.viewCornerRadius
        view.clipsToBounds = true
        return view
    }()
    
    private(set) lazy var fruitImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubviews()
        makeConstraints()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        addSubviews()
        makeConstraints()
    }
    
    func update(with fruit: FruitData) {
        fruitImageView.loadWithAlamofire(urlString: fruit.image)
    }
    
    private func addSubviews() {
        addSubview(view)
        view.addSubview(fruitImageView)
    }
    
    private func makeConstraints() {
        view.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        fruitImageView.snp.makeConstraints { (make) in
            make.width.equalTo(appearance.imageViewWidthHeight)
            make.height.equalTo(appearance.imageViewWidthHeight)
            make.center.equalToSuperview()
        }
    }
    
}
