//
//  AllFruitsPresenter.swift
//  FruitsApp
//
//  Created by Karim Razhanov on 23.02.2020.
//  Copyright © 2020 Karim Razhanov. All rights reserved.
//

import UIKit

protocol AllFruitsView: class {
    func setView()
    func showReloadAlert(errorText: String)
    func showAddAgainAlert(errorText: String, row: Int)
    func refreshFruitsData()
    func hideView()
}

protocol AllFruitsPresenterDelegate: class {
    func addNewFruit(_ fruit: FruitData)
}

protocol AllFruitsPresenter {
    var numberOfFruits: Int { get }
    func viewDidLoad()
    func fetchData()
    func configure(cell: FruitCollectionViewCell, forRow row: Int)
    func didSelect(row: Int)
}

class AllFruitsPresenterImplementation: AllFruitsPresenter {
    
    var fruits = [FruitData]()
    
    var numberOfFruits: Int {
        return fruits.count
    }
    
    fileprivate weak var view: AllFruitsView?
    
    fileprivate weak var delegate: AllFruitsPresenterDelegate?
    
    init(view: AllFruitsView, delegate: AllFruitsPresenterDelegate?) {
        self.view = view
        self.delegate = delegate
    }

    func viewDidLoad() {
        view?.setView()
        fetchData()
    }
    
    func fetchData() {
        FruitsService.getAllFruits { [weak self] (result) in
            switch result {
            case .success(let allFruits):
                self?.handleRecievedData(allFruits)
            case .failure(let error):
                self?.view?.showReloadAlert(errorText: error.localizedDescription)
            }
        }
    }
    
    fileprivate func handleRecievedData(_ fruits: [FruitData]) {
        self.fruits = fruits
        view?.refreshFruitsData()
    }
    
    func configure(cell: FruitCollectionViewCell, forRow row: Int) {
        cell.display(fruits[row])
    }
    
    func didSelect(row: Int) {
        FruitsService.addNewFruit(id: fruits[row].id) { (error) in
            if let error = error {
                self.view?.showAddAgainAlert(errorText: error.localizedDescription, row: row)
            } else {
                self.delegate?.addNewFruit(self.fruits[row])
                self.view?.hideView()
            }
        }
    }
}
