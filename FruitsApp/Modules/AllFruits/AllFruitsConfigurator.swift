//
//  AllFruitsConfigurator.swift
//  FruitsApp
//
//  Created by Karim Razhanov on 23.02.2020.
//  Copyright © 2020 Karim Razhanov. All rights reserved.
//

import UIKit

protocol AllFruitsConfigurator {
    func configure(allFruitsViewController: AllFruitsViewController)
}

class AllFruitsConfiguratorImplementation: AllFruitsConfigurator {
    var allFruitsPresenterDelegate: AllFruitsPresenterDelegate?
    
    init(allFruitsPresenterDelegate: AllFruitsPresenterDelegate?) {
        self.allFruitsPresenterDelegate = allFruitsPresenterDelegate
    }
    
    func configure(allFruitsViewController: AllFruitsViewController) {
        let presenter = AllFruitsPresenterImplementation(view: allFruitsViewController, delegate: allFruitsPresenterDelegate)
        allFruitsViewController.presenter = presenter
    }
}
