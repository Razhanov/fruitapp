//
//  FruitCollectionViewCell.swift
//  FruitsApp
//
//  Created by Karim Razhanov on 23.02.2020.
//  Copyright © 2020 Karim Razhanov. All rights reserved.
//

import UIKit

class FruitCollectionViewCell: UICollectionViewCell {
    
    private(set) lazy var fruitCellView: FruitCellView = {
        let fruitCellView = FruitCellView()
        return fruitCellView
    }()
    
    func display(_ fruitData: FruitData) {
        fruitCellView.update(with: fruitData)
        addSubviews()
        makeConstraints()
    }
    
    private func addSubviews() {
        addSubview(fruitCellView)
    }
    
    private func makeConstraints() {
        fruitCellView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
}
