//
//  AllFruitsViewController.swift
//  FruitsApp
//
//  Created by Karim Razhanov on 23.02.2020.
//  Copyright © 2020 Karim Razhanov. All rights reserved.
//

import UIKit
import UBottomSheet

extension AllFruitsViewController {
    struct Appearance {
        let topInset: CGFloat = 106
        let bottomYPercentage = 80 / UIScreen.main.bounds.height
        let titleFontSize: CGFloat = 21
        let collectionViewItemSize = CGSize(width: 100, height: 100)
        let collectionViewLefInset: CGFloat = 42
        let collectionViewRightInset: CGFloat = 40
        let viewCornerRadius: CGFloat = 20
        let headerViewHeight: CGFloat = 100
        let titleTopOffset: CGFloat = 30
    }
}

class AllFruitsViewController: BottomSheetController {
    
    var configurator: AllFruitsConfigurator?
    var presenter: AllFruitsPresenter?
    
    private let appearance = Appearance()
    
    override var initialPosition: SheetPosition {
        return .bottom
    }
    
    override var topInset: CGFloat {
        return appearance.topInset
    }
    
    override var bottomYPercentage: CGFloat {
        return appearance.bottomYPercentage
    }
    
    override var scrollView: UIScrollView? {
        return collectionView
    }
    
    private(set) lazy var headerView: UIView = {
        let view = UIView()
        view.backgroundColor = .allFruitsBackgroundColor
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapHeader(_:)))
        view.addGestureRecognizer(tap)
        return view
    }()
    
    private(set) lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = Constants.allFruitsText
        label.textColor = .black
        label.font = UIFont.roboto(ofSize: appearance.titleFontSize, style: .medium)
        label.textAlignment = .center
        return label
    }()
    
    private(set) lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = appearance.collectionViewItemSize
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 0, left: appearance.collectionViewLefInset, bottom: 0, right: appearance.collectionViewRightInset)
        let collectionView = UICollectionView(frame: CGRect(origin: .zero, size: CGSize(width: view.bounds.width, height: view.bounds.height)), collectionViewLayout: layout)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .allFruitsBackgroundColor
        collectionView.register(FruitCollectionViewCell.self, forCellWithReuseIdentifier: Constants.fruitCollectionViewCellIdentifier)
        return collectionView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        configurator?.configure(allFruitsViewController: self)
        presenter?.viewDidLoad()
    }
    
    @objc func tapHeader(_ uiTapGestureRecognizer: UITapGestureRecognizer) {
        changePosition(to: .top)
    }
    
}

extension AllFruitsViewController: AllFruitsView {
    func setView() {
        view.layer.cornerRadius = appearance.viewCornerRadius
        view.clipsToBounds = true
        
        view.addSubview(headerView)
        headerView.addSubview(titleLabel)
        view.addSubview(collectionView)
        headerView.snp.makeConstraints { (make) in
            make.leading.trailing.top.equalToSuperview()
            make.height.equalTo(appearance.headerViewHeight)
        }
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(appearance.titleTopOffset)
            make.centerX.equalToSuperview()
        }
        collectionView.snp.makeConstraints { (make) in
            make.top.equalTo(headerView.snp.bottom)
            make.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    func refreshFruitsData() {
        collectionView.reloadData()
    }
    
    func showReloadAlert(errorText: String) {
        let ac = UIAlertController(title: Constants.errorTitle, message: errorText, preferredStyle: .alert)
        let reloadAction = UIAlertAction(title: Constants.reloadText, style: .default) { [weak self] _ in
            self?.presenter?.fetchData()
        }
        ac.addAction(reloadAction)
        present(ac, animated: true, completion: nil)
    }
    
    func showAddAgainAlert(errorText: String, row: Int) {
        let ac = UIAlertController(title: Constants.addAgainAlertTitle, message: errorText, preferredStyle: .alert)
        let reloadAction = UIAlertAction(title: Constants.tryAgainText, style: .default) { [weak self] _ in
            self?.presenter?.didSelect(row: row)
        }
        ac.addAction(reloadAction)
        present(ac, animated: true, completion: nil)
    }
    
    func hideView() {
        changePosition(to: .bottom)
    }
}

extension AllFruitsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter?.numberOfFruits ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.fruitCollectionViewCellIdentifier, for: indexPath) as? FruitCollectionViewCell else {
            let cell = FruitCollectionViewCell()
            presenter?.configure(cell: cell, forRow: indexPath.row)
            return cell
        }
        presenter?.configure(cell: cell, forRow: indexPath.row)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter?.didSelect(row: indexPath.row)
    }
    
}

